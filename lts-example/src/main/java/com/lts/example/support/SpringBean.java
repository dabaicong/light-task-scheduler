package com.lts.example.support;

/**
 * 测试bean 注入
 * Created by hugui on 8/4/15.
 */
public class SpringBean {

    public void hello(){
        System.out.println("我是SpringBean，我执行了");
    }
}
